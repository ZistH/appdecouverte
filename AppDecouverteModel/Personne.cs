﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDecouverteModel
{
    public class Personne
    {
        public String nom { get; set; }

        public String prenom { get; set; }

        public override string ToString()
        {
            return prenom + " " + nom;
        }
    }
}
