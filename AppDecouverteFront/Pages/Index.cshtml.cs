using AppDecouverteModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AppDécouverteFront.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            Personne jojo = new Personne();

            jojo.nom = "AHAHA";
            jojo.prenom = "Jojo";

            ViewData["jojo"] = jojo;
        }
    }
}
